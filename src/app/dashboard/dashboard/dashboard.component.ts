import { Component, OnInit } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

export interface PeriodicElement {
  mask: string;
  position: number;
}

const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, mask: 'Hydrogen'},
  {position: 2, mask: 'Helium'},
  {position: 3, mask: 'Lithium'},
  {position: 4, mask: 'Beryllium'},
  {position: 5, mask: 'Boron'}
];

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  displayedColumns: string[] = ['position', 'mask', 'delete'];
  dataSource = ELEMENT_DATA;

  constructor(public dialog: MatDialog) { }

  ngOnInit() {
  }


}
