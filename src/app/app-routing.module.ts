import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard/dashboard.component';
import { EnterMobileNumberComponent } from './logins/enter-mobile-number/enter-mobile-number.component';
import { EnterOtpComponent } from './logins/enter-otp/enter-otp.component';
import { InitialBlockMaskComponent } from './logins/initial-block-mask/initial-block-mask.component';


export const routes: Routes = [
  {
    path: '',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent
  },
  {
    path: 'enter-mobile-number',
    component: EnterMobileNumberComponent
  },
  {
    path: 'enter-otp',
    component: EnterOtpComponent
  },
  {
    path: 'initial-block-mask',
    component: InitialBlockMaskComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
