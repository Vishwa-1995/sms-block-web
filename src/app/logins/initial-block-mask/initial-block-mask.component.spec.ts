import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitialBlockMaskComponent } from './initial-block-mask.component';

describe('InitialBlockMaskComponent', () => {
  let component: InitialBlockMaskComponent;
  let fixture: ComponentFixture<InitialBlockMaskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitialBlockMaskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitialBlockMaskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
