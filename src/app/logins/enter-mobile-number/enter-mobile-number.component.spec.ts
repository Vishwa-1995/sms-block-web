import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EnterMobileNumberComponent } from './enter-mobile-number.component';

describe('EnterMobileNumberComponent', () => {
  let component: EnterMobileNumberComponent;
  let fixture: ComponentFixture<EnterMobileNumberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EnterMobileNumberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EnterMobileNumberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
